class Edge:

    def __init__(self, name, path):
        self.name = name
        self.path = path
        self.left = None
        self.right = None

    def is_leaf(self):
        return not (self.left or self.right)

    def is_image(self, node):
        names_match = (node.name == self.name)
        status_match = (node.is_leaf() == self.is_leaf())
        return names_match and status_match


def deserialize_node(data, path=''):
    node = Edge(data['name'], path)
    if data['left'] and data['right']:
        node.left = deserialize_node(data['left'], path + '0')
        node.right = deserialize_node(data['right'], path + '1')
    return node


def find_image(tree, node, seen):
    if not tree:
        return None
    if tree.is_image(node) and tree.path not in seen:
        return tree
    return find_image(tree.left, node, seen) or find_image(tree.right, node, seen)


def o_trace(traced, tree):
    if not traced:
        return {}
    seen_nodes = set()
    image = None
    while not image:
        image = find_image(tree, traced, seen_nodes)
        if not image:
            return None
        trace_left = o_trace(traced.left, image) or o_trace(traced.left, image.left)
        trace_right = o_trace(traced.right, image) or o_trace(traced.right, image.right)
        if trace_left is None or trace_right is None:
            seen_nodes.add(image.path)
            image = None
    trace = {traced: image}
    trace.update(trace_left)
    trace.update(trace_right)
    return trace
