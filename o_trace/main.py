import json

from o_trace.edge import o_trace, deserialize_node

Z1_FILE_NAME = 'z1.json'
Z2_FILE_NAME = 'z2.json'


if __name__ == '__main__':
    with open(Z1_FILE_NAME, 'r') as file:
        z1 = deserialize_node(json.load(file))
    with open(Z2_FILE_NAME, 'r') as file:
        z2 = deserialize_node(json.load(file))
    trace = o_trace(z1, z2)
    print({node.path: image.path for node, image in trace.items()})
