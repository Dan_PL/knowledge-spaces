import json
from sys import argv

NAMES_SPLITTER = '#'
TRIPLES_SPLITTER = '$'


def parse(filename):
    with open(filename, 'r') as file:
        text = file.read()
    ontology = []
    triple_lines = text.split(TRIPLES_SPLITTER)
    for triple_line in triple_lines:
        ontology.append(triple_line.split(NAMES_SPLITTER))
    return ontology


def subjects(ontology, obj, rel):
    subjects_list = []
    for triple in ontology:
        if triple[1] == rel and triple[2] == obj:
            subjects_list.append(triple[0])
    return subjects_list


def recursive_an(ontology, obj, rels):
    if not rels:
        return obj
    rel = rels[0]
    new_rels = rels[1:]
    neighbours = []
    for subject in subjects(ontology, obj, rel):
        neighbours.append(recursive_an(ontology, subject, new_rels))
    an = {
        'object': obj,
        'relation': rel,
        'neighbours': neighbours
        }
    return an
    

def main():
    filename = argv[1]
    ontology = parse(filename)
    obj = argv[2]
    rels = [name for name in argv[3:]]
    with open('an.json', 'w') as file:
        file.write(json.dumps(recursive_an(ontology, obj, rels)))


if __name__ == '__main__':
    main()
